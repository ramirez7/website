/*-----------------------------------------------------------------------------
   Init

   Initialization of Styx, should not be edited
-----------------------------------------------------------------------------*/
{ styx
, extraConf ? {}
}:

rec {

  /* Importing styx library
  */
  styxLib = import styx.lib styx;


/*-----------------------------------------------------------------------------
   Themes setup

-----------------------------------------------------------------------------*/

  /* Importing styx themes from styx
  */
  styx-themes = import styx.themes;

  /* list the themes to load, paths or packages can be used
     items at the end of the list have higher priority
  */
  themes = [
  ];

  /* Loading the themes data
  */
  themesData = styxLib.themes.load {
    inherit styxLib themes;
    extraEnv = { inherit data pages; };
    extraConf = [ ./conf.nix extraConf ];
  };

  /* Bringing the themes data to the scope
  */
  inherit (themesData) conf lib files templates env;


/*-----------------------------------------------------------------------------
   Data

   This section declares the data used by the site
-----------------------------------------------------------------------------*/

  data = {
  };


/*-----------------------------------------------------------------------------
   Pages

   This section declares the pages that will be generated
-----------------------------------------------------------------------------*/

  pages = rec {
    index = { 
      layout   = template: "<html><body>${template}</body></html>"; 
      template = page: '' 
        <h2>Armando Ramirez's Website</h2>
        <p><b>Coming soon!</b></p>
        <p>Built with <a href="https://styx-static.github.io/styx-site/">Styx</a></p>
        <p><a href="/feed.rss">RSS Feed</a></p>
      '';
      path    = "/index.html"; 
    };

    rss = {
      layout = styxLib.id;
      template = _: ''
        <?xml version="1.0" encoding="UTF-8" ?>
        <rss version="2.0">

          <channel>
            <title>Armando's Website</title>
            <link>https://armandoramirez.dev</link>
            <description>Haskell, Nix, and more :)</description>
            <item>
              <title>Coming Soon!</title>
              <link>https://armandoramirez.dev</link>
              <description>Placeholder RSS item</description>
            </item>
          </channel>
        </rss>
      '';
      path = "/feed.rss";
    };
  };


/*-----------------------------------------------------------------------------
   Site

-----------------------------------------------------------------------------*/

  /* Converting the pages attribute set to a list
  */
  pageList = lib.pagesToList { inherit pages; };

  /* Generating the site
  */
  site = lib.mkSite { inherit files pageList; };

}
