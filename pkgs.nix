let
  rev = "9db41c12f584c456266bc89be2208bc527d09043";
  fork = "NixOS";
  sha256 = "0xf2a2c4vh59y28xl59hwy823ckbx78skzfag8v31lkfqsflzjqc";
  pkgs-src = builtins.fetchTarball {
    url = "https://github.com/${fork}/nixpkgs/archive/${rev}.tar.gz";
    sha256 = sha256;
  };
in
import pkgs-src
